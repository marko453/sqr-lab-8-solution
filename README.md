# Lab 8 - Rollbacks and Reliability

Student name: <b>Marko Pezer</b><br>
Student email: <b>m.pezer@innopolis.university</b><br>
Student group: <b>BS18-SE-01</b>

## Solution

Python code is in the `main.py` file. <br>
SQL query for `Inventory` table creation is in the `inventory_table_creation.txt` file. 
